﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace VipBracelets
{
    public class Bracelet
    {
        private List<Image> template;
        private Bitmap image;
        private Graphics graphics;

        public static SizeF Size { get; set; }
        public static PointF Margin { get; set; }
        public static float Padding { get; set; }
        public static Brush Foreground { get; set; }
        public static Color Background { get; set; }

        static Bracelet()
        {
            Size = new SizeF(25f, 1.5f);
            Margin = new PointF(1f, 0f);
            Padding = 0.5f;
            Foreground = Brushes.Black;
            Background = Color.Yellow;
        }

        public Bracelet()
        {
            template = new List<Image>();

            image = new Bitmap(Size.Width.ToPixels(), Size.Height.ToPixels(), PixelFormat.Format32bppArgb);
            image.SetResolution(ImageHelper.Dpi, ImageHelper.Dpi);

            graphics = Graphics.FromImage(image);
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PageUnit = GraphicsUnit.Pixel;
        }

        public Image Get()
        {
            graphics.Clear(Background);
            float maxHeight = (Size.Height - (Margin.Y * 2)).ToPixels();
            var nextLocation = new PointF(Margin.X.ToPixels(), Margin.Y.ToPixels());
            foreach (var component in template)
            {
                var factor = component.Height / maxHeight;
                SizeF size;
                PointF location;
                if (factor < 0)
                {
                    size = new SizeF(component.Width, component.Height);
                    location = new PointF(nextLocation.X, nextLocation.Y + ((maxHeight - component.Height) / 2));
                }
                else
                {
                    size = new SizeF(component.Width / factor, component.Height / factor);
                    location = new PointF(nextLocation.X, nextLocation.Y);
                }
                var bounds = new RectangleF(location, size);
                graphics.DrawImage(component, bounds);
                nextLocation = new PointF(nextLocation.X + size.Width + Padding.ToPixels(), Margin.Y.ToPixels());
            }
            return image;
        }

        public void AddImage(Image image)
        {
            template.Add(image);
        }

        public void AddText(string text, Font font = null)
        {
            if (font == null)
                font = new Font("Calibri", 72);
            var size = graphics.MeasureString(text, font);
            var image = new Bitmap((int)size.Width, (int)size.Height);
            image.SetResolution(ImageHelper.Dpi, ImageHelper.Dpi);
            var textGraphics = Graphics.FromImage(image);
            textGraphics.Clear(Background);
            textGraphics.DrawString(text, font, Foreground, 0, 0);
            AddImage(image);
        }

        public void AddQrCode(string content)
        {
            QrCode qrCode;
            Stream stream = null;
            try
            {
                var qrEncoder = new QrEncoder(ErrorCorrectionLevel.Q);
                int moduleSizeInPixels = 5;
                var renderer = new GraphicsRenderer(new FixedModuleSize(moduleSizeInPixels, QuietZoneModules.Two), Foreground, new SolidBrush(Background));
                qrCode = qrEncoder.Encode(content);
                stream = new MemoryStream();
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, stream);
                AddImage(Image.FromStream(stream));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }
    }
}
