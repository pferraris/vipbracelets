﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Linq;

namespace VipBracelets
{
    public class BraceletSet
    {
        private List<Bracelet> bracelets;

        public static SizeF Size { get; set; }
        public static PointF Margin { get; set; }
        public static float Padding { get; set; }
        public static SizeF WorkingSize { get { return new SizeF(Size.Width - Margin.X, Size.Height - Margin.Y); } }

        static BraceletSet()
        {
            Size = new SizeF(29.7f, 21.0f);
            Margin = new PointF(2f, 1.5f);
            Padding = 0.1f;
        }

        public int PageSize { get { return (int)Math.Floor(WorkingSize.Height / (Bracelet.Size.Height + Padding)); } }
        public int PageCount { get { return (int)Math.Ceiling(bracelets.Count / (float)PageSize); } }
        public int Count => bracelets.Count;

        public void Add(Bracelet bracelet)
        {
            bracelets.Add(bracelet);
        }

        public BraceletSet()
        {
            bracelets = new List<Bracelet>();
        }

        public IEnumerable<Image> GetAll()
        {
            for (int page = 1; page <= PageCount; page++)
                yield return GetOne(page);
        }

        public Image GetOne(int page)
        {
            if (page < 0) throw new ArgumentException("Page can't be less than 0");
            if (page > PageCount) throw new ArgumentException($"Page can't be greater than {PageCount}");

            Bitmap image = new Bitmap(Size.Width.ToPixels(), Size.Height.ToPixels(), PixelFormat.Format32bppArgb);
            image.SetResolution(ImageHelper.Dpi, ImageHelper.Dpi);

            Graphics graphics = Graphics.FromImage(image);
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PageUnit = GraphicsUnit.Pixel;

            FillPage(graphics, page);

            return image;
        }

        public void PrintAll()
        {
            for (int page = 1; page <= PageCount; page++)
                PrintOne(page);
        }

        public void PrintOne(int page)
        {
            if (page < 0) throw new ArgumentException("Page cant be less than 0");
            if (page > PageCount) throw new ArgumentException($"Page cant be greater than {PageCount}");

            var printer = new PrintDocument();
            printer.DefaultPageSettings.Landscape = true;
            printer.PrintPage += (sender, e) =>
            {
                e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                e.Graphics.PageUnit = GraphicsUnit.Pixel;
                FillPage(e.Graphics, page);
            };
            printer.Print();
        }

        private void FillPage(Graphics graphics, int page)
        {
            var nextLocation = new PointF(Margin.X.ToPixels(), Margin.Y.ToPixels());
            var size = new SizeF(Bracelet.Size.Width.ToPixels(), Bracelet.Size.Height.ToPixels());
            foreach (var bracelet in GetPage(page))
            {
                var bounds = new RectangleF(nextLocation, size);
                graphics.DrawImage(bracelet.Get(), bounds);
                nextLocation = new PointF(nextLocation.X, nextLocation.Y + size.Height + Padding);
            }
        }

        private IEnumerable<Bracelet> GetPage(int page)
        {
            return bracelets.Skip((page - 1) * PageSize).Take(PageSize);
        }
    }
}
