﻿using System;
using System.Drawing.Printing;

namespace VipBracelets
{
    public static class ImageHelper
    {
        public static float Dpi { get; set; }

        static ImageHelper()
        {
            Dpi = new PrintDocument().DefaultPageSettings.PrinterResolution.X;
        }

        public static int ToPixels(this float source)
        {
            var inchs = source / 2.54f;
            return (int)(Math.Ceiling(inchs * Dpi));
        }
    }
}
