﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace VipBracelets
{
    public static class CryptographyExtensions
    {
        public static string TripleDES(this string data, string key, string iv)
        {
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;
            try
            {
                byte[] content = Encoding.UTF8.GetBytes(data);
                byte[] IV = Encoding.UTF8.GetBytes(iv);
                byte[] Key = Encoding.UTF8.GetBytes(key);
                msEncrypt = new MemoryStream();
                var encryptor = new TripleDESCryptoServiceProvider().CreateEncryptor(Key, IV);
                csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                csEncrypt.Write(content, 0, content.Length);
                csEncrypt.FlushFinalBlock();
                return Convert.ToBase64String(msEncrypt.ToArray());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (csEncrypt != null)
                {
                    csEncrypt.Close();
                    csEncrypt.Dispose();
                }
                if (msEncrypt != null)
                {
                    msEncrypt.Close();
                    msEncrypt.Dispose();
                }
            }
        }
    }
}
