﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using VipBracelets;

namespace VipBraceletsGenerator
{
    class Program
    {
        private const string IV = "z1a3q5w7s9x0c8d6e4r2f2v5";
        private const string Key = "m2k4o6i8j0n9b7h5u3y1g5v7";

        static void Main(string[] args)
        {
            var personalData = new
            {
                Id = 12345678,
                FullName = "Natalio Rodriguez",
                Sucursal = "Belgrano 123456",
            };
            var content = JsonConvert.SerializeObject(personalData);
            content = content.TripleDES(Key, IV);

            var bracelet = new Bracelet();
            bracelet.AddImage(Image.FromFile(@"C:\Users\pablo\Downloads\Farmacity_logo.png"));
            bracelet.AddText("Fiesta Fin de Año 2016", new Font("Verdana", 10));
            bracelet.AddQrCode(content);
            bracelet.AddText("1546");

            var braceletSet = new BraceletSet();
            braceletSet.Add(bracelet);

            braceletSet.PrintAll();

            //int index = 0;
            //foreach (var page in braceletSet.GetAll())
            //{
            //    var fileName = $"page{++index}.png";
            //    page.Save(fileName, ImageFormat.Png);
            //    Process.Start(fileName);
            //}
        }
    }
}
